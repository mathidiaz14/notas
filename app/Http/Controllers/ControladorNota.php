<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Nota;
use Auth;

class ControladorNota extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $nota               = new Nota();
        $nota->contenido    = encrypt($request->nota);
        $nota->codigo       = $this->codigo_aleatorio();
        $nota->color        = $request->color;
        $nota->publico      = $request->publico;
        $nota->save();

        Auth::user()->notas()->attach($nota->id);

        Session(['status' => "Se creo la nota"]);

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $nota = Nota::where('codigo', $id)->first();

        if(($nota != null) and ($nota->publico == "on"))
            return view('show', compact('nota'));

        return "No se puede acceder a la nota";
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $nota               = Nota::find($id);
        $nota->contenido    = encrypt($request->nota);
        $nota->color        = $request->color;
        $nota->publico      = $request->publico;
        $nota->save();

        Session(['status' => "Se modifico la nota"]);

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $nota = Nota::find($id);
        
        if($nota->usuarios->count() == 1)
            $nota->delete();
        else
            $nota->usuarios()->detach(Auth::user()->id);

        Session(['status' => "La nota se elimino correctamente"]);

        return back();

    }

    private function codigo_aleatorio($cantidad = 20)
    {
        return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $cantidad); 
    }
}
