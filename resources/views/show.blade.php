@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12 notas p-4">
            <div class="row">
                
                <div class="col-12 nota" style="background: {{$nota->color}};">
                    <textarea name="" id="" cols="30" rows="10" class="form-control" readonly="" style="background: none;">
                        {{decrypt($nota->contenido)}}
                    </textarea>
                    <hr>
                    <div class="row">
                        <div class="col">
                            @if($nota->publico == "on")
                                <a href="{{url('nota', $nota->codigo)}}" target="_blank"><small>{{$nota->codigo}}</small></a>
                            @else
                                <small class="text-secondary">{{$nota->codigo}}</small>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
    <script>
        var myModal = document.getElementById('exampleModal');
        var myInput = document.getElementById('nota');
        

        myModal.addEventListener('shown.bs.modal', function () {
            myInput.focus();
        });
    </script>   
@endsection