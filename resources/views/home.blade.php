@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12 text-end">
            
            <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
                <i class="fa fa-plus"></i>
                Agregar nota
            </button>

            <!-- Modal -->
            <div class="modal fade text-start" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Nueva nota</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <form action="{{url('nota')}}" class="form-horizontal" method="post">
                                @csrf
                                <div class="mb-3">
                                    <textarea name="nota" id="nota" cols="30" rows="10" class="form-control" placeholder="Escriba su nota"></textarea>
                                </div>
                                <div class="mb-3">
                                    <div class="row">
                                        <div class="col">
                                            <input type="color" style="width: 100%;" class="form-control form-control-color" name="color" value="#95c2e4" title="Choose your color">
                                        </div>
                                        <div class="col">
                                            <div class="form-check">
                                                <label class="form-check-label" for="publico">
                                                    ¿Permitir acceso publico?
                                                </label>
                                                <input class="form-check-input" type="checkbox" id="publico" name="publico">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="mb-3 text-end">
                                    <hr>
                                    <button class="btn btn-primary">
                                        <i class="fa fa-save"></i>
                                        Guardar
                                    </button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
        </div>
        <div class="col-12 notas p-4">
            <div class="row">
                @foreach($notas as $nota)
                    <div class="col-12 col-md-4 col-lg-3 p-2">
                        <div class="nota"  style="background: {{$nota->color}};">
                            <textarea name="" id="" cols="30" rows="10" class="form-control" readonly="" style="background: none; border:none;">{{decrypt($nota->contenido)}}</textarea>
                            <hr>
                            <div class="row">
                                <div class="col">
                                    @if($nota->publico == "on")
                                        <a href="{{url('publico', $nota->codigo)}}" target="_blank"><small>{{$nota->codigo}}</small></a>
                                    @else
                                        <small class="text-secondary">{{$nota->codigo}}</small>
                                    @endif
                                </div>
                                <div class="col text-end">
                                    @include('partes.compartir', ['nota' => $nota])

                                    @include('partes.editar', ['nota' => $nota])

                                    @include('partes.eliminar', ['nota' => $nota])                                
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
    <script>
        var myModal = document.getElementById('exampleModal');
        var myInput = document.getElementById('nota');
        

        myModal.addEventListener('shown.bs.modal', function () {
            myInput.focus();
        });
    </script>   
@endsection