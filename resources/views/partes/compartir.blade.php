<button type="button" class="btn btn-info" data-bs-toggle="modal" data-bs-target="#compartirModal_{{$nota->id}}">
    <i class="fa fa-share-alt"></i>
</button>

<!-- Modal -->
<div class="modal fade text-start" id="compartirModal_{{$nota->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Compartitr nota</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                @if($nota->usuarios->where('email', '!=', Auth::user()->email)->count() == 0)
                    <p>Esta nota no esta compartida</p>
                @else
                    <p>Compartida con:</p>
                    <div class="table table-responsive">
                        <table class="table table-striped">
                            @foreach($nota->usuarios->where('email', '!=', Auth::user()->email) as $usuario)
                                <tr>
                                    <td>{{$usuario->email}}</td>
                                    <td>
                                        <form action="{{url('compartir')}}" method="post">
                                            @csrf
                                            @method('delete')
                                            <button class="btn btn-danger">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                @endif
                <hr>
                <form action="{{url('compartir')}}" class="form-horizontal" method="post">
                    @csrf
                    <input type="hidden" name="nota" value="{{$nota->id}}">
                    <div class="row">
                        <div class="col-10">
                            <input type="text" class="form-control" name="email" placeholder="Email del usuario a comparir">
                        </div>
                        <div class="col-2">
                            <button class="btn btn-primary">
                                <i class="fa fa-send"></i>
                                Enviar
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>