
    <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#editarModal_{{$nota->id}}">
        <i class="fa fa-edit"></i>
    </button>

    <!-- Modal -->
    <div class="modal fade text-start" id="editarModal_{{$nota->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Nueva nota</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form action="{{url('nota', $nota->id)}}" class="form-horizontal" method="post">
                        @csrf
                        @method('PATCH')
                        <div class="mb-3">
                            <textarea name="nota" id="nota" cols="30" rows="10" class="form-control" placeholder="Escriba su nota">{{decrypt($nota->contenido)}}</textarea>
                        </div>
                        <div class="mb-3">
                            <div class="row">
                                <div class="col">
                                    <input type="color" style="width: 100%;" class="form-control form-control-color" name="color" value="{{$nota->color}}" title="Choose your color">
                                </div>
                                <div class="col">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" id="publico_{{$nota->id}}" name="publico" @if($nota->publico == "on") checked @endif >
                                        <label class="form-check-label" for="publico_{{$nota->id}}">
                                            ¿Permitir acceso publico?
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="mb-3 text-end">
                            <hr>
                            <button class="btn btn-primary">
                                <i class="fa fa-save"></i>
                                Guardar
                            </button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>