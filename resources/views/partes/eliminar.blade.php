<button type="button" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#eliminar_{{$nota->id}}">
    <i class="fa fa-trash"></i>
</button>

<!-- Modal -->
<div class="modal fade" id="eliminar_{{$nota->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header bg-danger bg-gradient">
                <h5 class="modal-title" id="exampleModalLabel">Eliminar elemento</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body text-center">
                <p><i class="fa fa-exclamation-triangle fa-4x"></i></p>
                <h4>¿Desea eliminar el item?</h4>
                <br>
                <hr>
                <div class="col-12">
                    <form action="{{ url('nota',$nota->id) }}" method="POST">
                        @csrf
                        <input type='hidden' name='_method' value='DELETE'>
                        <button class="btn btn-danger ">
                            Eliminar nota
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>