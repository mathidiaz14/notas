<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('home');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->middleware('auth');

Route::get('publico/{ID}', [App\Http\Controllers\ControladorNota::class, 'show']);

Route::resource('nota', App\Http\Controllers\ControladorNota::class)->middleware('auth');
Route::resource('compartir', App\Http\Controllers\ControladorCompartir::class)->middleware('auth');